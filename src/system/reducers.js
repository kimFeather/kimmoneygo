import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import userReducers from "../reducers/userReducer";


const reducers = history =>
    combineReducers({
        userAccount: userReducers,
        router: connectRouter(history)
    })

export default reducers