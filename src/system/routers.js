import React, { Component } from 'react'
import { Route, Switch } from 'react-router-native'
import { ROUTE_TO_LOGIN, ROUTE_TO_REGISTER } from "../constants/routeConstant";
import { LoginPage } from "../containers/index.js";

export default class Router extends Component {
    render() {
        return (
            <Switch>
                <Route exact path={ROUTE_TO_LOGIN} component={LoginPage} />
            </Switch>
        )
    }
}