// Define action that used in redux

// ---------- User actions  ----------
export const saveCurrentUser = ({ username, password, firstname, lastname, token }) => ({
    type: SAVE_CURRENT_USER,
    payload: { username, password, firstname, lastname, token },
})